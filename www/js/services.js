angular.module('ppostracker.services', [])
    .factory('AuthService', function ($q, $http, $rootScope, $state) {
        'use strict';
        var STORE_DATA_KEY = 'ppos_dashboard_store_data';
        var CURRENT_STORE_KEY = 'ppos_dashboard_current_store';

        var stores = undefined;
        var currentStore = {};


        function loadStoredData() {
            var storeData = window.localStorage.getItem(STORE_DATA_KEY);
            if (storeData) {
                stores = JSON.parse(storeData);
            } else {
                stores = [];
            }

            var currentStoreData = window.localStorage.getItem(CURRENT_STORE_KEY);
            if (currentStoreData) {
                currentStore = JSON.parse(currentStoreData);
            }
        }

        function findStoreByName(storeName) {
            for (var i = 0; i < stores.length; i++) {
                if (stores[i].store == storeName) {
                    return i;
                }
            }
            return -1;
        }

        function findStoreByToken(token) {
            for (var i = 0; i < stores.length; i++) {
                if (stores[i].token == token) {
                    return i;
                }
            }
            return -1;
        }

        function removeToken(token) {
            var index = findStoreByToken(token);
            if (index > -1) {
                stores.splice(index, 1);
                window.localStorage.setItem(STORE_DATA_KEY, JSON.stringify(stores));
                if (isSetCurrentStore()) {
                    if (currentStore.token == token) {
                        var temp = currentStore;
                        currentStore = {};
                        window.localStorage.setItem(CURRENT_STORE_KEY, JSON.stringify(currentStore));
                        $rootScope.$broadcast("currentStoreRemoved", temp);
                    }
                }
            }
        }

        function saveStore(store) {
            var index = findStoreByName(store.store);
            if (index == -1) {
                stores.push(store);
                window.localStorage.setItem(STORE_DATA_KEY, JSON.stringify(stores));
                //$rootScope.$broadcast("storeAdded", store);
                $rootScope.$broadcast('storeAdded', store);
                //console.log('Fired event storeAdded (store: '+store.store+', state: '+$state.current.name+')');
            }
        }

        function removeStore(storeName) {
            var index = findStoreByName(storeName);
            if (index > -1) {
                stores.splice(index, 1);
                window.localStorage.setItem(STORE_DATA_KEY, JSON.stringify(stores));

                if (isSetCurrentStore()) {
                    if (currentStore.store == storeName) {
                        var temp = currentStore;
                        currentStore = {};
                        window.localStorage.setItem(CURRENT_STORE_KEY, JSON.stringify(currentStore));

                    }
                }
            }
        }

        function isSetCurrentStore() {
            return currentStore && currentStore.hasOwnProperty('store');
        }

        function removeCurrentStore() {
            if (isSetCurrentStore()) {
                var index = findStoreByName(currentStore.store);
                if (index > -1) {
                    var temp = currentStore;
                    stores.splice(index, 1);
                    window.localStorage.setItem(STORE_DATA_KEY, JSON.stringify(stores));
                    currentStore = {};
                    window.localStorage.setItem(CURRENT_STORE_KEY, JSON.stringify(currentStore));
                    $rootScope.$broadcast("currentStoreRemoved", temp);
                }
            }
        }

        function setCurrentStore(storeName) {
            if (isSetCurrentStore()) {
                if (currentStore.store == storeName)
                    return;
            }
            var index = findStoreByName(storeName);
            if (index > -1) {
                currentStore = stores[index];
                window.localStorage.setItem(CURRENT_STORE_KEY, JSON.stringify(currentStore));
                $rootScope.$broadcast('storeChanged', {store: currentStore, state: $state.current.name});
            }
        }

        var login = function (store, name, pw, uuid, model) {
            return $q(function (resolve, reject) {
                var data = {
                    username: name.toLowerCase(),
                    password: pw,
                    store: store.toLowerCase(),
                    uuid: uuid,
                    model: model
                };
                $http.post(APP_CONFIG.api_url + 'authenticate', data).then(function (resp) {

                    if (typeof resp.data.code !== 'undefined') {
                        if (resp.data.code == 0) {
                            var storeInfo = {
                                store: resp.data.store,
                                user: resp.data.user,
                                token: resp.data.token
                            };

                            saveStore(storeInfo);
                            //setCurrentStore(storeInfo);
                            resolve({code: 0, message: 'Login success'});
                        } else {
                            reject(resp.data);
                        }

                    } else {
                        reject({code: 603, message: 'Authentication failed'});
                    }
                }, function (err) {
                    //alert(JSON.stringify(err));
                    reject({code: 602, message: 'Authentication failed'});
                })
            });
        };

        var deleteSession = function (token) {
            return $q(function (resolve, reject) {
                $http.put(APP_CONFIG.api_url + 'sessions/', {delete_token: token}).then(function (resp) {
                    if (typeof resp.data.code !== 'undefined') {
                        if (resp.data.code == 0) {
                            removeToken(token);
                            resolve(resp.data);
                        } else {
                            reject(resp);
                        }
                    } else {
                        reject(resp);
                    }
                }, function (err) {
                    //console.log(JSON.stringify(err));
                })
            });

        };


        var validateStoreToken = function (storeName) {
            var _self = this;
            var index = findStoreByName(storeName);
            var token = undefined;
            if (index > -1) {
                token = stores[index].token;
            }
            return $q(function (resolve, reject) {
                var self = _self;


                $http.get(APP_CONFIG.api_url + 'ping/', {
                    headers: {'x-access-token': token}
                }).then(function (resp) {
                    if (typeof resp.data.code !== 'undefined') {
                        if (resp.data.code == 0) {
                            resolve(resp.data);
                        } else {
                            reject(resp);
                        }
                    } else {
                        reject(resp);
                    }
                }, function (err) {
                    /*if (err.hasOwnProperty('data')) {
                     if (err.data.hasOwnProperty('code')) {
                     if (err.data.code == '823' || err.data.code == '824') {
                     reject(err.data);
                     return;
                     }
                     }
                     }*/
                    reject({code: 911, message: 'Error validating store'});
                })

            });

        };
        var getSessions = function () {
            return $q(function (resolve, reject) {

                $http.get(APP_CONFIG.api_url + 'sessions/').then(function (resp) {
                    if (typeof resp.data.code !== 'undefined') {
                        if (resp.data.code == 0) {
                            resolve(resp.data);
                        } else {
                            reject(resp);
                        }
                    } else {
                        reject(resp);
                    }
                    // For JSON responses, resp.data contains the result
                }, function (err) {
                    reject(err);
                })
            });
        };


        loadStoredData();

        return {
            login: login,
            removeStore: removeStore,
            removeCurrentStore: removeCurrentStore,
            setCurrentStore: setCurrentStore,
            getSessions: getSessions,
            deleteSession: deleteSession,
            getStores: function () {
                return angular.copy(stores);
            },
            getCurrentStore: function () {
                return angular.copy(currentStore);
            },
            validateStoreToken: validateStoreToken
        };

    })

    .service('APIInterceptor', function ($rootScope, $injector, $q) {
        var service = this;
        var numLoadings = 0;

        service.request = function (config) {
            var authService = $injector.get("AuthService");

            var currentStore = authService.getCurrentStore(),
                access_token = currentStore ? currentStore.token : null;

            if (access_token) {
                config.headers['x-access-token'] = access_token;
            }
            // Show loader
            if (config.url.indexOf('http') >= 0) {
                numLoadings++;
                $rootScope.$broadcast("showLoader", {});
            }
            return config || $q.when(config);
        };

        service.response = function (response) {

            if (response.data.hasOwnProperty('code')) {
                if ((numLoadings - 1) >= 0) {
                    // Hide loader
                    numLoadings--;
                    $rootScope.$broadcast("hideLoader", {});
                }
            }

            return response || $q.when(response);
        };

        service.responseError = function (rejection) {

            if ((numLoadings - 1) >= 0) {
                numLoadings--;
                $rootScope.$broadcast("hideLoader", {});
            }

            if (rejection.status === 401) {
                if (rejection.hasOwnProperty('data')) {
                    if (rejection.data.hasOwnProperty('code')) {
                        if (rejection.data.code == '823') {
                            var authService = $injector.get("AuthService");
                            authService.removeCurrentStore();
                            return $q.reject(rejection);
                        }

                    }
                }

                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
            }
            if (rejection.status === 404) {
                if (rejection.hasOwnProperty('data')) {
                    if (rejection.data.hasOwnProperty('code')) {
                        if (rejection.data.code == '811') {
                            $rootScope.$broadcast('storeNotFound');
                        }

                    }
                }
            }


            return $q.reject(rejection);
        };
    })
    .service("$soap", ['$q', function ($q) {
        return {
            post: function (url, action, params) {
                var deferred = $q.defer();

                //Create SOAPClientParameters
                var soapParams = new SOAPClientParameters();
                for (var param in params) {
                    soapParams.add(param, params[param]);
                    console.log(params);
                }

                //Create Callback
                var soapCallback = function (e) {
                    if (e.constructor.toString().indexOf("function Error()") != -1) {
                        deferred.reject("An error has occurred.");
                    } else {
                        deferred.resolve(e);
                    }
                }

                SOAPClient.invoke(url, action, soapParams, true, soapCallback);

                return deferred.promise;
            }
        }
    }])
    .factory("appService", function ($soap, $q, APP_CONFIG) {
        var generateFakeData = function (code) {

            return $q(function (resolve, reject) {
                var errorMsg = 'null';
                var errorCode = 'null';
                var statusMsg = 'null';
                var itemName = 'null';

                switch (code) {
                    case '1235R56390abea1933R2820':
                    {
                        errorMsg = 'OK';
                        errorCode = 0;
                        itemName = 'Samsung Metro';
                        statusMsg = 'rv';
                        break;
                    }
                    case '1235R562e67ea7d29aR2820':
                    {
                        errorMsg = 'OK';
                        errorCode = 0;
                        itemName = 'iPhone 5S Gold';
                        statusMsg = 'rv';
                        break;
                    }
                    case '1235R56670d70356acR4826':
                    {
                        errorMsg = 'OK';
                        errorCode = 0;
                        itemName = 'iPhone 5S White';
                        statusMsg = 'dl';
                        break;
                    }
                    case '1235R5667240a0f4f6R4827':
                    {
                        errorMsg = 'OK';
                        errorCode = 0;
                        itemName = 'Galaxy S5';
                        statusMsg = 'rp';
                        break;
                    }
                    case '1235R562285f1281b7R4489':
                    {
                        errorMsg = 'OK';
                        errorCode = 0;
                        itemName = 'Samsug s3 t mobile';
                        statusMsg = 'cf';
                        break;
                    }
                    default:
                    {
                        itemName = 'Galaxy S5';
                        if (code.indexOf('1') == 0 || code.indexOf('2') == 0) {
                            errorMsg = 'OK';
                            errorCode = 0;

                            if (code.indexOf('1') == 0) {
                                statusMsg = 'rv';

                            } else {
                                if (code.indexOf('2') == 0) {
                                    statusMsg = 'cf';
                                }
                            }

                        } else {
                            errorMsg = 'Invalid tracking code';
                            errorCode = 1;
                        }
                        break;
                    }

                }


                var data = {
                    storeName: 'Flager',
                    storeAddr: '1279 West Flagler St Miami Florida 33135',
                    storeContactPhone: '(305) 922-6773',
                    itemName: itemName,
                    statusId: 'null',
                    statusMsg: statusMsg,
                    errorCode: errorCode,
                    errorMsg: errorMsg
                }
                setTimeout(function () {
                    resolve(data);
                }, 10000);
            });

        };

        var getStatus = function (code) {

            // return generateFakeData(code);
            return $soap.post(APP_CONFIG.ws_url, "repairTracking", {repairCode: code});
        };

        return {
            getStatus: getStatus
        };
    })
    .factory("DB", function ($rootScope, $cordovaSQLite, $q) {

        var getAllTracking = function () {
            return $q(function (resolve, reject) {
                $cordovaSQLite.execute($rootScope.db, "SELECT * FROM tracking")
                    .then(function (res) {
                            var data = [];
                            for (var i = 0; i < res.rows.length; i++) {
                                var item = res.rows.item(i);
                                item['current_action'] = 'showing';
                                data.push(item);
                            }
                            resolve(data);
                        },
                        function (err) {
                            reject(err);
                        });
            });
        };

        var insertTracking = function (data) {
            return $q(function (resolve, reject) {
                var sql = "INSERT INTO tracking(item_code,item_name,item_status,store_name,store_addr,store_phone,updated_at) VALUES (?,?,?,?,?,?,?)";
                var now = Date.now();
                $cordovaSQLite.execute($rootScope.db, sql, [data.itemCode, data.itemName, data.statusMsg, data.storeName, data.storeAddr, data.storeContactPhone, now])
                    .then(function (res) {
                            resolve({
                                item_code: data.itemCode,
                                item_name: data.itemName,
                                item_status: data.statusMsg,
                                store_name: data.storeName,
                                store_addr: data.storeAddr,
                                store_phone: data.storeContactPhone,
                                current_action: 'showing',
                                updated_at: now
                            });
                        },
                        function (err) {
                            reject(err);
                        });
            });
        };

        var updateTracking = function (data) {
            return $q(function (resolve, reject) {
                var sql = "UPDATE tracking set item_status=?,updated_at=? where item_code=?";
                var now = Date.now();
                $cordovaSQLite.execute($rootScope.db, sql, [data.statusMsg, now, data.itemCode])
                    .then(function (res) {
                            resolve({
                                item_code: data.itemCode,
                                item_name: data.itemName,
                                item_status: data.statusMsg,
                                store_name: data.storeName,
                                store_addr: data.storeAddr,
                                current_action: 'showing',
                                store_phone: data.storeContactPhone,
                                updated_at: now
                            });
                        },
                        function (err) {
                            reject(err);
                        });
            });
        };

        var deleteTracking = function (code) {
            return $q(function (resolve, reject) {
                var sql = "DELETE FROM tracking WHERE item_code=?";
                var now = Date.now();
                $cordovaSQLite.execute($rootScope.db, sql, [code])
                    .then(function (res) {
                            resolve({
                                errorCode: 0,
                                itemCode: code
                            });
                        },
                        function (err) {
                            reject(err);
                        });
            });
        };


        return {
            getAllTracking: getAllTracking,
            insertTracking: insertTracking,
            deleteTracking: deleteTracking,
            updateTracking: updateTracking
        };
    });


