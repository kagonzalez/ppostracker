angular.module('ppostracker.controllers', [])

    .controller('AppCtrl', function ($scope, $rootScope, $ionicLoading, $ionicPopup, $ionicPlatform, $ionicPopover,$cordovaBarcodeScanner, appService, DB) {

        $scope.data = {};

        var showSpinner = function (flag) {
            if (flag) {
                $ionicLoading.show({template: '<ion-spinner icon="bubbles"></ion-spinner>'});
            } else {
                $ionicLoading.hide();
            }
        };

        $scope.searchCode = function () {
            if ($scope.data.code && $scope.existTracking($scope.data.code) == false) {
                showSpinner(true);
                appService.getStatus($scope.data.code).then(function (result) {
                    showSpinner(false);
                    if (result.hasOwnProperty('errorCode')) {
                        if (result.errorCode == '0') {
                            result.itemCode = $scope.data.code;
                            DB.insertTracking(result).then(function (item) {
                                $scope.items.push(item);
                                $scope.showAdd();
                            }, function (err) {
                                console.log(err);
                            });
                        } else {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Error',
                                template: result.errorMsg
                            });
                        }
                    }


                }, function (err) {
                    showSpinner(false);
                    console.log(err);
                });

            }
        };

        $scope.scanBarcode = function () {
            $cordovaBarcodeScanner.scan().then(function (imageData) {
                if (!imageData.cancelled) {
                    $scope.data.code = imageData.text;
                }
            }, function (error) {

            });
        };


        $ionicPopover.fromTemplateUrl('templates/actions.html', {
            scope: $scope,
        }).then(function (popover) {
            $scope.actionsPopover = popover;
        });


        $scope.showActions = function ($event, code) {
            $scope.selectedCode = code;
            $scope.actionsPopover.show($event);
        };

        $scope.updateTracking = function (code) {
            var index = $scope.getItemIndex(code);
            if (index >= 0) {
                var item = $scope.items[index];
                item['current_action'] = 'updating';

                appService.getStatus(code).then(function (result) {

                    if (result.hasOwnProperty('errorCode')) {
                        if (result.errorCode == '0') {
                            result.itemCode = code;
                            DB.updateTracking(result).then(function (item) {
                                var index = $scope.getItemIndex(code);
                                var it = $scope.items[index];
                                it['updated_at'] = item.updated_at;
                                it['current_action'] = item.current_action;
                                it['item_status'] = item.item_status;
                            }, function (err) {
                                console.log(err);
                            });
                        } else {
                            var index = $scope.getItemIndex(code);
                            var it = $scope.items[index];
                            it['current_action'] = 'deleting';
                        }
                    }

                }, function (err) {
                    console.log(err);
                });
            }

        };

        $scope.refreshSingle = function () {
            $scope.actionsPopover.hide();
            if ($scope.selectedCode) {
                $scope.updateTracking($scope.selectedCode);
            }
        };

        $scope.refreshAll = function () {
            for (var i = 0; i < $scope.items.length; i++) {
                $scope.updateTracking($scope.items[i].item_code);
            }
        };

        $scope.deleteSingle = function () {
            $scope.actionsPopover.hide();
            if ($scope.selectedCode) {
                showSpinner(true);
                DB.deleteTracking($scope.selectedCode).then(function (item) {
                    showSpinner(false);
                    $scope.removeTrackingFromMemory($scope.selectedCode);
                }, function (err) {
                    showSpinner(false);
                    console.log(err);
                });
            }
        };

        $scope.deleteTracking = function (code) {
            if (code) {
                showSpinner(true);
                DB.deleteTracking(code).then(function (item) {
                    showSpinner(false);
                    $scope.removeTrackingFromMemory(code);
                }, function (err) {
                    showSpinner(false);
                    console.log(err);
                });
            }
        };

        $scope.existTracking = function (code) {
            return $scope.getItemIndex(code) >= 0;
        };

        $scope.getItemIndex = function (code) {
            for (var i = 0; i < $scope.items.length; i++) {
                if ($scope.items[i].item_code == code) {
                    return i;
                }
            }
            return -1;
        };

        $scope.removeTrackingFromMemory = function (code) {
            for (var i = 0; i < $scope.items.length; i++) {
                if ($scope.items[i].item_code == code) {
                    $scope.items.splice(i, 1);
                    break;
                }
            }
        };

        $scope.loadStoredTracking = function () {
            DB.getAllTracking().then(function (data) {
                $scope.items = data;
                $scope.refreshAll();
            }, function (err) {
                console.log(err);
            });
        };

        $scope.showAdd = function(){
                    if (window.AdMob) {
                        window.AdMob.prepareInterstitial({
                            adId: 'ca-app-pub-7029998053713357/4140668428',
                            autoShow: true
                        });
                    }
        };


        $scope.$on('$ionicView.enter', function () {
            $ionicPlatform.ready(function () {
                $scope.showAdd();
                $scope.loadStoredTracking();
            });
        });


    })

