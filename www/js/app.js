// Ionic Starter App

angular.module('ppostracker', ['ionic', 'ngCordova', 'ngIOS9UIWebViewPatch','ppostracker.controllers','ppostracker.services'])

    .constant('APP_CONFIG', {
        //ws_url: 'http://localhost:8100/ws'
        ws_url: 'https://smanager.procellpos.com/app/RepairApi'
    })
    .filter('statusName',function(){
        return function(status){
            if(status){
                switch(status){
                    case 'rv': return 'Received';
                    case 'rp': return 'Repaired';
                    case 'cf': return 'Can\'t fix';
                    case 'dl': return 'Delivered';

                }
                return  'Unknown';

            }
            return 'Unknown';
        }
    })

    .run(function ($ionicPlatform, $rootScope,$cordovaSQLite) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }


          $rootScope.db = $cordovaSQLite.openDB("data.db");
         // $rootScope.db = window.openDatabase("data2.db", "1.1", "Cordova Demo", 200000);
            $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS tracking(item_code text primary key, item_name text, item_status text, store_name text, store_addr text, store_phone text, updated_at timestamp)");

        });
    })

    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

        $ionicConfigProvider.views.maxCache(0);
        $ionicConfigProvider.views.transition('none');
       // $ionicConfigProvider.scrolling.jsScrolling(false);


        $stateProvider
            .state('mainx', {
                url: '/',
                templateUrl: 'templates/main.html',
                controller: 'AppCtrl'
                /*views:{
                    main:{

                    }
                }*/

            })   ;
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/');

    });

